class ApiException(BaseException):
    def __init__(self, message):
        super().__init__(message)
        self.message = message


class WrongCredentials(ApiException):
    pass


class DocumentDoesntExist(ApiException):
    pass
