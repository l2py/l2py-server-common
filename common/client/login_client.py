from common.client.client import ApiClient


class LoginClient(ApiClient):

    def __init__(self, server_ip, server_port):
        server_ip = server_ip
        server_port = server_port
        super().__init__(server_ip, server_port)

    async def auth_login(self, login, login_ok1, login_ok2, play_ok1, play_ok2):
        return await self.post("game/auth_login", json_data={
            "login": login.value,
            "login_ok1": login_ok1.value,
            "login_ok2": login_ok2.value,
            "play_ok1": play_ok1.value,
            "play_ok2": play_ok2.value,
        })
