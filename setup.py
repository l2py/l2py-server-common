import time
from setuptools import setup, find_packages


setup(name="l2py-server-common",
      packages=find_packages(),
      version=f"0.{int(time.time())}",
      install_requires=["aiohttp"])
